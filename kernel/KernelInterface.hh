<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\kernel
{
	interface KernelInterface
	{
		public function getLoader():ClassLoaderInterface;
		public function getClassManager():ClassManagerInterface;
		public function getExceptionHandler():ExceptionBootstrapperInterface;
	}
}
