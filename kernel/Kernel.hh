<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\kernel
{
	use nuclio\exception\NuclioException;
	use nuclio\core\
	{
		loader\ClassLoader,
		ClassManager,
		plugin\PluginInterface
	};
	use nuclio\kernel\ClassManagerInterface;
	
	type KernelParams=shape
	(
		'classLoader'		=>ClassLoader,
		'classManager'		=>ClassManagerInterface,
		'eventManager'		=>EventManagerInterface,
		'exceptionHandler'	=>ExceptionBootstrapperInterface,
	);
	
	/**
	 * Nuclio's base kernel class.
	 * 
	 * The purpose of a kernel in Nuclio is to provide the foundation
	 * for any programmer to develop an application from.
	 * 
	 * Wether it be a web based application or a CLI application, the kernel
	 * is a good boilerplate to make any application.
	 * 
	 * Primarily, the kernel provides the following:
	 * 
	 * * Class Manager
	 * 		- The ability to retreive instances of classes base on that
	 * 		classes AOP rules.
	 * 		- Register and track instances of classes.
	 * * Class Loader
	 * 		- The ability to dynamially load classes.
	 * 		- Load classes only when they're needed (JIT Loading).
	 * * Event Manager
	 * 		- Dispatcher
	 * * Exception Handler Bootstrapper
	 * 		- 
	 * 		- 
	 * 
	 * 
	 * You can pass in any implementation of the above, so long as they
	 * implement the correct Nuclio interfaces. These interfaces will
	 * allow the Kernel to identify and interact with them correctly.
	 * 
	 * 
	 * @abstract
	 */
	abstract class Kernel implements KernelInterface
	{
		protected ClassLoader						$loader;
		protected ClassManagerInterface				$classManager;
		protected EventManagerInterface				$eventManager;
		protected ExceptionBootstrapperInterface	$exceptionHandler;
		
		public function __construct(KernelParams $params)
		{
			$this->loader			=$params['classLoader'];
			$this->classManager		=$params['classManager'];
			$this->eventManager		=$params['eventManager'];
			$this->exceptionHandler	=$params['exceptionHandler'];
			
			// call_user_method('init',$this->exceptionHandler);
		}
		
		public function getLoader():ClassLoaderInterface
		{
			return $this->loader;
		}
		
		public function getClassManager():ClassManagerInterface
		{
			return $this->classManager;
		}
		
		public function getExceptionHandler():ExceptionBootstrapperInterface
		{
			return $this->exceptionHandler;
		}
		
		public function getEventManager():EventManagerInterface
		{
			return $this->eventManager;
		}
		
		public function getPlugin(string $plugin, /* HH_FIXME[4033] */...$args):PluginInterface
		{
			$instance=$this->classManager->createInstance($plugin,...$args);
			if ($instance instanceof PluginInterface)
			{
				return $instance;
			}
			else throw new NuclioException('Invalid Plugin! "'.$plugin.'"');
		}
	}
}
