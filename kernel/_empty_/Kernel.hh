<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\kernel\_empty_
{
	
	require_once('ClassLoader.hh');
	require_once('ClassManager.hh');
	
	use nuclio\kernel\
	{
		KernelInterface,
		ClassLoaderInterface,
		ClassManagerInterface,
		ExceptionBootstrapperInterface
	};
	
	/**
	 * Empty Kernel Interface.
	 * 
	 * Don't use this.
	 * 
	 * Ever.
	 * 
	 * You have been warned.
	 */
	class Kernel implements KernelInterface
	{
		protected ClassLoaderInterface $loader;
		protected ClassManagerInterface $manager;
		protected ExceptionBootstrapperInterface $exceptionHandler;
		
		public function __construct()
		{
			$this->loader			=new ClassLoader();
			$this->manager			=new ClassManager();
			$this->exceptionHandler	=new ExceptionBootstrapper();
		}
		
		public function getLoader():ClassLoaderInterface
		{
			return $this->loader;
		}
		
		public function getClassManager():ClassManagerInterface
		{
			return $this->manager;
		}
		
		public function getExceptionHandler():ExceptionBootstrapperInterface
		{
			return $this->exceptionHandler;
		}
	}
}
