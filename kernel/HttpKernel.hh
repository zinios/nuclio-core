<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\kernel
{
	use nuclio\core\exception\NuclioException;
	use nuclio\core\loader\ClassLoader;
	// use nuclio\core\ClassManager;
	
	type HttpKernelParams=shape
	(
		'classLoader'		=>ClassLoader,
		'classManager'		=>ClassManagerInterface,
		'eventManager'		=>EventManagerInterface,
		'exceptionHandler'	=>ExceptionBootstrapperInterface
	);
	
	/**
	 * Nuclio's HTTP kernel class.
	 * 
	 * 
	 * @abstract
	 */
	<<ignore>>
	class HttpKernel extends Kernel
	{
		public function __construct(HttpKernelParams $params)
		{
			parent::__construct($params);
		}
	}
}
