<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\exception
{
	use nuclio\Nuclio;
	use nuclio\kernel\ExceptionBootstrapperInterface;
	use Whoops\Run as WhoopsRunner;
	use Whoops\Handler\PrettyPageHandler;
	
	<<ignore>>
	class ExceptionBootstrapper implements ExceptionBootstrapperInterface
	{
		// private ?WhoopsRunner $whoops;
		// private ?PrettyPageHandler $handler;
		
		const string MODE_WEB	='web';
		const string MODEL_CLI	='cli';
		
		public function __construct(string $mode=self::MODE_WEB)
		{
			if ($mode==self::MODE_WEB)
			{
				$handler=new PrettyPageHandler();
				
				$handler->addDataTable
				(
					'Nuclio Info',
					[
						'Version'=>Nuclio::VERSION
					]
				);
				$whoops=new WhoopsRunner();
				$whoops->pushHandler($handler);
				$whoops->register();
			}
			
			
			
			// $this->whoops->pushHandler
			// (
			// 	function ($exception, $inspector, $whoops)
			// 	{
			// 		$inspector->getFrames()->map
			// 		(
			// 			function ($frame)
			// 			{
			// 				if ($function = $frame->getFunction())
			// 				{
			// 					$frame->addComment("This frame is within function '$function'", 'cpt-obvious');
			// 				}
			// 				return $frame;
			// 			}
			// 		);
			// 	}
			// );
			
			
		}
	}
}
