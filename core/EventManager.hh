<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core
{
	use nuclio\exception\NuclioException;
	use nuclio\kernel\EventManagerInterface;
	use nuclio\plugin\event\dispatcher\Dispatcher;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	
	<<ignore>>
	class EventManager implements EventManagerInterface
	{
		private static ?EventManager $instance=null;
		private static Dispatcher $dispatcher;
		
		public static function getInstance():EventManager
		{
			if (is_null(self::$instance))
			{
				self::$instance=new self();
			}
			return self::$instance;
		}
		
		public function __construct()
		{
			try
			{
				$dispatcher=ProviderManager::request('event::dispatcher');
			}
			catch (NuclioException $exception)
			{
				$dispatcher=Dispatcher::getInstance();
			}
			$this->dispatcher=$dispatcher;
		}
		
		public function getDispatcher():Dispatcher
		{
			return $this->dispatcher;
		}
	}
}
