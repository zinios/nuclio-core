<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core\loader
{
	function load(string $classPath):mixed
	{
		return require_once($classPath);
	}
	
	function composerLoad(string $classPath):mixed
	{
		$loader=require(ROOT_DIR.'vendor'._DS_.'autoload.php');
		if (is_object($loader))
		{
			$loader->loadClass($classPath);
		}
		return $loader;
	}
}
