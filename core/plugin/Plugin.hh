<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core\plugin
{
	use nuclio\Nuclio;
	use nuclio\core\ClassManager;
	use nuclio\core\Overloadable;
	use nuclio\core\plugin\PluginException;
	use nuclio\plugin\event\dispatcher\Dispatcher;
	
	abstract class Plugin implements PluginInterface,Overloadable
	{
		private ?Nuclio $nuclio;
		protected ?ClassManager $classManager=null;
		private ?string $lastKey;
		private Vector<string> $validOverloaders=Vector
		{
			'nuclio',
			'classManager',
			'plugin'
		};
		
		public function __construct()
		{
			$classManager	=Nuclio::getClassManager();
			$this->nuclio	=Nuclio::getInstance();
			if ($classManager instanceof ClassManager)
			{
				$this->classManager=$classManager;
			}
		}
		
		public function __call(string $method,array<mixed> $args):mixed
		{
			throw new PluginException('Overloading Exception. Undefined method "'.$method.'".');
			return null;
		}
		
		public function getDispatcher():?Dispatcher
		{
			return $this->nuclio?->getKernel()?->getEventManager()->getDispatcher();
		}
	}
}

