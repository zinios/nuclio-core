<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\core
{
	use nuclio\exception\NuclioException;
	
	function newClassInstance(string $className, ...$args):mixed
	{
		if (class_exists($className))
		{
			return new $className(...$args);
		}
		else
		{
			if (strstr($className,'/'))
			{
				throw new NuclioException(sprintf('It looks like a path has been passed to the loader
				instead of a class reference. "%s" is not a class reference.',$className));
			}
			else
			{
				throw new NuclioException(sprintf('"%s" has not been loaded.',$className));
			}
		}
	}
}
