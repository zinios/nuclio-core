<?hh //strict
namespace {{ plugin.namespace }}
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	
	<<{{ plugin.pattern }}>>
	class {{ plugin.name }} extends Plugin
	{
		public static function getInstance(/* HH_FIXME[4033] */ ...$args):{{ plugin.name }}
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		/**
		 * Constructor for plugin.
		 * 
		 * If you're doing any custom bootstrapping, remember to
		 * call the parent constructor.
		 * 
		 * @param {{ plugin.name }} $args Args array.
		 */
		public function __construct(/* HH_FIXME[4033] */ ...$args)
		{
			parent::__construct(...$args);
			
			//Other bootstrapping...
		}
	}
}