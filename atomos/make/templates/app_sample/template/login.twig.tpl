{% if installedTestUser == false %}
<p><a href="#" id="installTestUser">Install Test User</a></p>
{% else %}
<p>(try logging in with test@example.com/test)</p>
{% endif %}
<p id="message"></p>
<form id="loginForm">
	<label>Email: <input type="text" name="email" /></label><br />
	<label>Password: <input type="password" name="password" /></label><br />
	<input type="button" value="Login">
</form>
<div id="home">
<a href="/">Go Back</a>
</div>
<script type="text/javascript">
$(
	function()
	{
		var	loginForm		=$('#loginForm'),
			button			=loginForm.find('input[type=button]'),
			message			=$('#message'),
			home			=$('#home'),
			installTestUser	=$('#installTestUser');
		button.click
		(
			function()
			{
				$.post
				(
					'/api/example/login.json',
					{
						'email':	loginForm.find('input[name=email]').val(),
						'password':	loginForm.find('input[name=password]').val()
					},
					function(response)
					{
						if (response.success)
						{
							loginForm.hide();
							home.show();
						}
						message.html(response.message);
					}
				);
			}
		);
		installTestUser.click
		(
			function(e)
			{
				e.preventDefault();
				$.post
				(
					'/api/example/installTestUser.json',
					function(response)
					{
						if (response.success)
						{
							installTestUser.hide();
							alert('Test user was installed. Try logging in.');
							message.html("(try logging in with test@example.com/test)");
						}
						else
						{
							alert(response.message);
						}
					}
				);
			}
		);
	}
);
</script>