<?hh //strict
namespace {{ application.namespace }}\controller
{
	use nuclio\plugin\
	{
		application\controller\Controller,
		application\controller\Passable,
		auth\Auth,
		user\model\User
	};
	
	class Index extends Controller
	{
		use Passable;
		
		public function _index():void
		{
			/*
				"passControlTo" is a method provided by the Passable trait.
				
				It is a way for a controller to instruct another controller
				(usually a Viewable controller) to process some data and
				return the result as a string.
				
				Below we pass controll to another controller named "IndexView".
				
				IndexView uses the Viewable controller and is acting as a traditional
				view from an MVC.
				
				We pass a map of data which is prepared by this controller.
				Why?
				Because we consider any Viewable controller as "dumb". It should
				only react to data received and use that data to process a template,
				then return the compiled template as a string.
			 */
			print $this->passControlTo
			(
				'{{ application.namespace }}\\controller\\view\\IndexView',
				'run',
				Map
				{
					'content'		=>'home.twig.tpl',
					'authenticated'	=>Auth::getInstance()->isAuthenticated()
				}
			);
		}
		
		public function login():void
		{
			//We only want to show the login page if the user is not authenticated.
			if (!Auth::getInstance()->isAuthenticated())
			{
				$installedTestUser=false;
				if (User::findOne(Map{'ref'=>'testUser'}))
				{
					$installedTestUser=true;
				}
				print $this->passControlTo
				(
					'{{ application.namespace }}\\controller\\view\\IndexView',
					'run',
					Map
					{
						'content'			=>'login.twig.tpl',
						'installedTestUser'	=>$installedTestUser
					}
				);
			}
			//Already authenticated. Send the user to the homepage.
			else
			{
				$this->redirect('/');
			}
		}
		
		public function logout():void
		{
			//Unauthenticate the user.
			Auth::getInstance()->unauthenticate();
			//Reload the page.
			$this->redirect('/');
		}
	}
}