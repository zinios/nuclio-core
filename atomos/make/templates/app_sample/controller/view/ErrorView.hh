<?hh //strict
namespace {{ application.namespace }}\controller\view
{
	use nuclio\plugin\application\controller\Controller;
	use nuclio\plugin\application\controller\Viewable;
	
	class ErrorView extends Controller
	{
		use Viewable;
		
		public function _404(Map<string,mixed> $data):string
		{
			$this->set('content','404.twig.tpl');
			return $this->render('index.twig.tpl');
		}
		
		public function _405(Map<string,mixed> $data):string
		{
			$this->set('content','405.twig.tpl');
			return $this->render('index.twig.tpl');
		}
		
		public function _500(Map<string,mixed> $data):string
		{
			$this->set('content','500.twig.tpl');
			return $this->render('index.twig.tpl');
		}
	}
}