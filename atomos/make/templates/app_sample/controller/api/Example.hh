<?hh //strict
namespace {{ application.namespace }}\controller\api
{
	use {{ application.namespace }}\{{ application.name }};
	use nuclio\plugin\
	{
		application\controller\Controller,
		application\controller\Restful,
		user\User,
		user\UserException,
		auth\Auth,
		auth\AuthException,
		session\Session
	};
	
	/**
	 * This class uses the "Restful" trait. This trait provides some
	 * basic restful methods.
	 * 
	 * The main methods that we use are "setResponseCode" and "respond".
	 * One sets a HTTP response code wile the other sets the response body.
	 * 
	 * The response is handled in a RESTful way. So depending on what the client
	 * requests, depends on the output of the result.
	 * 
	 * For example. Calling "/login.json" will respond with a json document.
	 */
	class Example extends Controller
	{
		use Restful;
		
		public function installTestUser():void
		{
			//Fetch the policy information
			$policyConfig={{ application.name }}::getInstance()->getConfig()->get('policy');
			try
			{
				/*
					Create an instance of the user plugin and pass
					the policy config to it.
					
					This enforces constraints around the password.
					You could also pull this information from a database.
				 */
				User::getInstance($policyConfig)->create
				(
					Map
					{
						'email'				=>'test@example.com',
						'password'			=>'test',
						'password_confirm'	=>'test',
						'ref'				=>'testUser'
					}
				);
				$this->setResponseCode(200);
				$this->respond(true,'Success');
			}
			catch (UserException $exception)
			{
				$this->setResponseCode(200);
				$this->respond(false,$exception->getMessage());
			}
		}
		
		public function login():void
		{
			$auth=Auth::getInstance();
			
			//If already authenticated, ignore this login request.
			if ($auth->isAuthenticated())
			{
				$this->setResponseCode(200);
				$this->respond(true,'Already logged in');
			}
			
			//Pull out the post information from the request handler.
			$request	=$this->getRequest();
			$email		=$request->getPost('email');
			$password	=$request->getPost('password');
			
			try
			{
				/*
					Try to authenticate the user. An exception will
					be thrown if the request fails.
				 */
				$auth->authenticate($email,$password);
				$this->setResponseCode(200);
				$this->respond(true,'Success');
			}
			catch (AuthException $exception)
			{
				$this->setResponseCode(200);
				$this->respond(false,'Invalid login');
			}
		}
	}
}
