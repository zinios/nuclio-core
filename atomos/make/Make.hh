<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\atomos\make
{
	require_once(ROOT_DIR.'vendor/intaglio/nuclio-plugin-config/types.hh');
	
	use nuclio\plugin\console\
	{
		application\Application,
		Command,
		ConsoleException
	};
	use nuclio\plugin\
	{
		provider\manager\Manager as ProviderManager,
		config\Config,
		system\process\Process,
		http\request\Request,
		annotations\reader\Reader as AnnotationsReader,
		database\orm\ORM,
		database\datasource\manager\Manager as DataSourceManager,
		format\reader\Reader as FileReader
	};
	use nuclio\helper\
	{
		ObjectHelper,
		CryptHelper
	};
	use \ReflectionClass;
	use nuclio\plugin\format\driver\template\twig\Twig;
	use nuclio\plugin\config\ConfigCollection;
	
	/**
	 * Atomos Make Tool.
	 * 
	 * make::app
	 * make::plugin
	 * 
	 * 
	 * 
	 */
	class Make
	{
		private Vector<string> $fileList=Vector
		{
			'App.hh',
			'config/application.neon'
		};
		
		private Twig $twig;
		
		private Vector<string> $sampleFileList=Vector
		{
			'App.hh',
			'config/application.neon',
			'controller/api/Example.hh',
			'controller/view/ErrorView.hh',
			'controller/view/IndexView.hh',
			'controller/Error.hh',
			'controller/Index.hh'
		};
		
		public function __construct()
		{
			$this->twig=Twig::getInstance
			(
				Map
				{
					'debug'=>true,
					'cache'=>false
				}
			);
			$templateDir=__DIR__.'/templates/'._DS_;
			$this->twig->addPath($templateDir);
		}
		
		public function app(Command $command):void
		{
			$name		=$command->getArgumentValue('name') |> lcfirst($$);
			$initFile	=$command->getArgumentValue('init') ?? ROOT_DIR.'init.hh';
			$autoInit	=$command->getArgumentValue('autoInit') ?? true;
			$sample		=$command->getArgumentValue('sample');
			$appDir		=realpath(ROOT_DIR)._DS_.$name._DS_;
			$className	=ucfirst($name);
			
			if (!is_dir($appDir))
			{
				//Make app dir.
				mkdir($appDir);
			}
			else
			{
				//Prompt to overwrite.
				//TODO
				
			}
			
			$this->twig->addPath($appDir);
			
			if ($sample)
			{
				$fromDir=__DIR__.'/templates/app_sample/*';
				$fileList=$this->sampleFileList;
			}
			else
			{
				$fromDir=__DIR__.'/templates/app/*';
				$fileList=$this->fileList;
			}
			$cmd=sprintf('cp -R %s %s',$fromDir,$appDir);
			exec($cmd);
			
			foreach ($fileList as $file)
			{
				$fullFileName=$appDir.$file;
				//Load the file.
				$appContents=file_get_contents($fullFileName);
				
				//Compile the template.
				$this->twig->render
				(
					$file,
					Map
					{
						'application'=>
						[
							'namespace'	=>lcfirst($name),
							'name'		=>ucfirst($name)
						]
					}
				)
				//Rewrite the file.
				|> file_put_contents($fullFileName,$$);
			}
			
			//Rename the file.
			$appFileName=$appDir.ucfirst($name).'.hh';
			rename($appDir.'App.hh',$appFileName);
			
			//Write into composer.json
			$this->writePSR4($name.'\\',$name.'/');
			
			//Write to init.hh
			$this->createInitFile($name,$initFile,$autoInit);
			
			//Write the index.hh if it doesn't already exist.
			$this->createIndex();
			
			//Regen autoloader.
			$this->rebuildAutoloader();
			
			//Run Nuclio setup to optimize composer.
			$this->runNuclioSetup();
		}
		
		public function plugin(Command $command):void
		{
			$name				=$command->getArgumentValue('name') |> lcfirst($$);
			$app				=$command->getArgumentValue('app') ?? false;
			$singleton			=$command->getArgumentValue('singleton') ?? false;
			$factory			=$command->getArgumentValue('factory') ?? false;
			$pluginDir			=realpath(ROOT_DIR)._DS_.$name._DS_;
			$relativePluginDir	=$name._DS_;
			
			if (strstr($name,'\\'))
			{
				// $middle	=explode('\\',$name);
				// $first	=array_shift($middle) |> lcfirst($$);
				// $last	=array_pop($middle) |> ucfirst($$);
				// $middle	=array_map
				// (
				// 	($array)==>lcfirst($array),
				// 	$middle
				// )
				// |> implode('\\',$$);
				// $className	=implode('\\',[$first,$middle,$last]);
				$className	=explode('\\',$name)
				|> array_pop($$)
				|> ucfirst($$);
			}
			else
			{
				$className	=ucfirst($name);
			}
			
			if ($app)
			{
				$parts			=explode('\\',$app);
				array_pop($parts);
				$baseNamespace	=implode('\\',$parts).'\\';
				$appDir			=ROOT_DIR.implode('/',$parts)._DS_;
				$relativeAppDir	=implode('/',$parts)._DS_;
				$parts			=null;
				
				if (!is_dir($appDir))
				{
					throw new ConsoleException('Invalid application. To use this command, the application should exist in root.');
				}
				
				$middle	=explode('\\',$name);
				$first	=array_shift($middle) |> lcfirst($$);
				$last	=array_pop($middle) |> lcfirst($$);
				$middle	=array_map
				(
					($array)==>lcfirst($array),
					$middle
				)
				|> implode('\\',$$);
				
				$implodeArray=[];
				if ($middle!='')
				{
					$implodeArray=['plugin',$first,$middle,$last];
				}
				else
				{
					$implodeArray=['plugin',$first,$last];
				}
				$normalizedNamespace=implode('\\',$implodeArray);
				
				$namespace			=$baseNamespace.$normalizedNamespace;
				$pluginDir			=str_replace('\\','/',$appDir.$normalizedNamespace)._DS_;
				$relativePluginDir	=str_replace('\\','/',$relativeAppDir.$normalizedNamespace)._DS_;
			}
			else
			{
				$pluginDir			=ROOT_DIR.'plugin'._DS_;
				$relativePluginDir	.='plugin'._DS_;
				$namespace			='plugin\\'.$name;
			}
			
			// $pluginDir			.=$namespace._DS_ |> str_replace('\\','/',$$);
			// $relativePluginDir	.=$namespace._DS_ |> str_replace('\\','/',$$);
			
			// var_dump($pluginDir);
			// exit();
			if (!is_dir($pluginDir))
			{
				mkdir($pluginDir,0777,true);
			}
			
			//Handle singleton/factory.
			if ((!$singleton && !$factory)
			|| ($singleton && !$factory))
			{
				$pattern='singleton';
			}
			else if (!$singleton && $factory)
			{
				$pattern='factory';
			}
			else if ($singleton && $factory)
			{
				throw new ConsoleException('A plugin cannot be both a singleton and a factory.');
			}
			
			$fullFileName=$pluginDir.$className.'.hh';
						
			//Compile the template.
			$this->twig->render
			(
				'plugin/Plugin.hh',
				Map
				{
					'plugin'=>
					[
						'namespace'	=>$namespace,
						'name'		=>$className,
						'pattern'	=>$pattern
					]
				}
			)
			//Rewrite the file.
			|> file_put_contents($fullFileName,$$);
			
			//Write into composer.json
			$this->writePSR4($namespace.'\\',$relativePluginDir);
			
			//Regen autoloader.
			$this->rebuildAutoloader();
			
			//Run Nuclio setup to optimize composer.
			$this->runNuclioSetup();
		}
		
		public function regenInit(Command $command):void
		{
			$initMap=require_once(ROOT_DIR.'init.hh');
			
			$composerMap=FileReader::getContent(ROOT_DIR.'composer.json');
			$psr4=$composerMap?->get('autoload')?->get('psr-4');
			if (!is_null($psr4))
			{
				foreach ($psr4 as $namespace=>$directory)
				{
					if (!is_dir(ROOT_DIR.$directory))
					{
						$appName=rtrim($namespace,'\\') |> ucfirst($$);
						
						//Find it in the init Map.
						if ($initMap->containsKey($namespace.$appName))
						{
							//Found, remove it.
							$initMap->remove($namespace.$appName);
						}
					}
				}
			}
			//else nothing to clean!
			
			$this->writeInitFile($initMap);
		}
		
		
		
		
		
		
		
		
		
		
		
		
		private function writePSR4(string $namespace,string $path):void
		{
			$composerMap=FileReader::getContent(ROOT_DIR.'composer.json');
			if (!$composerMap->containsKey('autoload'))
			{
				$composerMap->set('autoload',Map{});
			}
			$autoload=$composerMap->get('autoload');
			
			if (!$autoload->containsKey('psr-4'))
			{
				$autoload->set('psr-4',Map{});
				$composerMap->set('autoload',$autoload);
			}
			$psr4=$autoload->get('psr-4');
			
			if (!$psr4 instanceof Map)
			{
				$psr4=Map{};
			}
			$psr4->set($namespace,$path);
			$autoload->set('psr-4',$psr4);
			
			json_encode($composerMap)
			|> $this->prettyPrint($$)
			|> trim($$)
			|> file_put_contents(ROOT_DIR.'composer.json',$$);
		}
		
		private function createInitFile(string $name, ?string $initFile=null, bool $autoInit=true):void
		{
			$fullName=lcfirst($name).'\\'.ucfirst($name);
			if (!is_file($initFile))
			{
				$initMap=Map{};
			}
			else if ($initFile)
			{
				$initMap=require_once($initFile);
			}
			else
			{
				$initMap=require_once(ROOT_DIR.'init.hh');
			}
			
			if (!$initMap instanceof Map)
			{
				throw new ConsoleException('Invalid init.hh config. Expected a Map.');	
			}
			
			$newConfig=Map{'autoInit'=>$autoInit};
			
			if (!$initMap->containsKey($fullName))
			{
				$initMap->set($fullName,$newConfig);
			}
			else
			{
				$initMap->get($fullName);
				if (!$initMap instanceof Map)
				{
					throw new ConsoleException('Invalid init.hh config. Expected a Map.');	
				}
				$initMap->set($fullName,$newConfig);
			}
			
			$this->writeInitFile($initMap);
		}
		
		private function writeInitFile(Map<string,Map<string,mixed>> $initMap):void
		{
			//Export as valid Hack.
			var_export($initMap,true)
			//Render into init.hh template.
			|>	$this->twig->render
				(
					'init.twig.hh',
					Map{'map'=>$$}
				)
			//Rewrite the file.
			|> file_put_contents(ROOT_DIR.'init.hh',$$);
		}
		
		private function createIndex():void
		{
			if (!is_file(ROOT_DIR.'index.hh'))
			{
				if (!is_dir(ROOT_DIR.'public'))
				{
					mkdir(ROOT_DIR.'public');
				}
				copy(__DIR__.'/templates/index.hh',ROOT_DIR.'public/index.hh');
			}
		}
		
		private function runNuclioSetup():void
		{
			//TODO: "atomos optimize"
			exec('hhvm '.ROOT_DIR.'vendor/bin/nuclio setup');
		}
		
		private function rebuildAutoloader():void
		{
			exec('composer dumpautoload');
		}
		
		private function prettyPrint($json):string
		{
			$result = '';
			$level = 0;
			$in_quotes = false;
			$in_escape = false;
			$ends_line_level = null;
			$json_length = strlen( $json );

			for( $i = 0; $i < $json_length; $i++ ) {
				$char = $json[$i];
				$new_line_level = null;
				$post = "";
				if( $ends_line_level !== null ) {
					$new_line_level = $ends_line_level;
					$ends_line_level = null;
				}
				if ( $in_escape ) {
					$in_escape = false;
				} else if( $char === '"' ) {
					$in_quotes = !$in_quotes;
				} else if( ! $in_quotes ) {
					switch( $char ) {
						case '}': case ']':
							$level--;
							$ends_line_level = null;
							$new_line_level = $level;
							break;

						case '{': case '[':
							$level++;
							$ends_line_level = null;
							$new_line_level = $level-1;
						case ',':
							$ends_line_level = $level;
							break;

						case ':':
							$post = " ";
							break;

						case " ": case "\t": case "\n": case "\r":
							$char = "";
							$ends_line_level = $new_line_level;
							$new_line_level = null;
							break;
					}
				} else if ( $char === '\\' ) {
					$in_escape = true;
				}
				if( $new_line_level !== null ) {
					$result .= "\n".str_repeat( "\t", $new_line_level );
				}
				$result .= $char.$post;
			}

			return $result;
		}
	}
}