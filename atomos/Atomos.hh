<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\atomos
{
	use nuclio\plugin\console\
	{
		application\Application,
		Command,
		ConsoleException
	};
	use nuclio\plugin\
	{
		provider\manager\Manager as ProviderManager,
		config\Config,
		system\process\Process,
		http\request\Request,
		annotations\reader\Reader,
		database\orm\ORM,
		database\datasource\manager\Manager as DataSourceManager
	};
	use nuclio\helper\
	{
		ObjectHelper,
		CryptHelper,
		load //Not working!?
	};
	use \ReflectionClass;
	
	/**
	 * Atomos command line tool.
	 * 
	 * key::generate
	 * actions::map
	 * 
	 * make::app
	 * make::plugin
	 * 
	 * 
	 * 
	 * 
	 */
	class Atomos extends Application
	{
		public function onMapCommands():void
		{
			$this->loadCommandsFromFile(__DIR__.'/commands.neon');
		}
	}
}