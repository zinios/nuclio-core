<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace
{
	use nuclio\plugin\console\
	{
		application\Application,
		Command,
		ConsoleException
	};
	use nuclio\plugin\
	{
		provider\manager\Manager as ProviderManager,
		config\Config,
		system\process\Process,
		http\request\Request,
		annotations\reader\Reader,
		database\orm\ORM,
		database\datasource\manager\Manager as DataSourceManager
	};
	use nuclio\helper\
	{
		ObjectHelper,
		CryptHelper,
		load //Not working!?
	};
	use \ReflectionClass;
	use \PHPUnit_TextUI_Command;
	
	class Common extends Application
	{
		const string DEFAULT_USER		='support';
		const string DEFAULT_GROUP		='support';
		
		private Map<string,string> $diffedClasses=Map{};
		
		<<
			command('setup'),
			arguments
			(
				[
					['--config','-c'],
					'The path to the config file you to use for this command.',
					false
				],
				[
					['--user','-u'],
					'The user to apply setup permissions with.',
					false
				],
				[
					['--group','-g'],
					'The group to apply setup permissions with.',
					false
				],
				[
					['--rebuild','-r'],
					'Rebuilds the provider and actions cache.',
					false
				],
				[
					['--regen-keypair','-rk'],
					'Rebuilds the provider and actions cache.',
					false
				]
			)
		>>
		public function setup(Command $command):void
		{
			$configPath		=($command->getArgumentValue('config') ?? NUC_DEFUALT_CONFIG_DIR)
							|> realpath($$);
			$user			=$command->getArgumentValue('user') ?? self::DEFAULT_USER;
			$group			=$command->getArgumentValue('group') ?? self::DEFAULT_GROUP;
			$rebuild		=$command->getArgumentValue('rebuild') ?? false;
			$regenKeypair	=$command->getArgumentValue('regen-keypair') ?? false;
			$request		=Request::getInstance();
			
			if (!is_writable(ROOT_DIR))
			{
				$rootDir=realpath(ROOT_DIR);
				print <<<MESSAGE
\e[;31mThere is a problem creating the nuclio temp directory. I cannot write to it.
\e[m\e[1;41m* Please check the permissions for {$rootDir}.
* Try running this command as sudo.
* Note that you may need to specify the --user and/or --group flags if you want Nuclio to configure the permissions for you.\e[m

MESSAGE;
				exit(1);
			}
			if (!$user)
			{
				$user=self::DEFAULT_USER;
			}
			if (!$group)
			{
				$group=self::DEFAULT_GROUP;
			}
			$this->createNUCTempDir($user,$group);
			$this->createNUCLogsDir($user,$group);
			$this->createNUCCacheDir($user,$group);
			
			$vendorDir='\''.realpath(ROOT_DIR).'/vendor';
			
			file_get_contents(ROOT_DIR.'vendor/composer/autoload_classmap.php')
			|> str_replace
			(
				[
					'$vendorDir . \'',
					'$baseDir . \''
				],
				[$vendorDir,dirname($vendorDir)],
				$$
			)
			|> file_put_contents(ROOT_DIR.'vendor/composer/autoload_classmap.php',$$);
			
			$autoloadReal	=file(ROOT_DIR.'vendor/composer/autoload_real.php');
			$newReal		=[];
			for ($i=0,$j=count($autoloadReal); $i<$j; $i++)
			{
				if (!strstr($autoloadReal[$i],'require $file;'))
				{
					$newReal[]=$autoloadReal[$i];
				}
				else
				{
					$newReal[]=sprintf('%srequire_once($file);%s',"\t",PHP_EOL);
				}
			}
			file_put_contents(ROOT_DIR.'vendor/composer/autoload_real.php',implode('',$newReal));
			
			//Map providers.
			$providersFile=NUC_TMP_DIR.'providers.map';
			if (!is_file($providersFile) || $rebuild)
			{
				if (!$rebuild)
				{
					print "\033[0;33mProviders have not been mapped. Mapping providers...\033[0m".PHP_EOL;
				}
				else
				{
					print "\033[0;33mRemapping providers...\033[0m".PHP_EOL;
				}
				if (is_file($providersFile) && !is_writable($providersFile))
				{
					throw new ConsoleException(sprintf('The providers cachefile at "%s" is not writable.',$providersFile));
				}
				$manager=ProviderManager::getInstance();
				$manager->bindPaths(Vector{ROOT_DIR});
				$providers=$manager->searchForProviders(true);
				if (json_encode($providers) |> file_put_contents($providersFile,$$)!==false)
				{
					print "\033[0;32mProviders Mapped!\033[0m".PHP_EOL;
				}
				else
				{
					print 'Oops! Something went wrong. Failed to write to disk. Could be permission related or the disk might be full.'.PHP_EOL;
					exit(1);
				}
			}
			
			//Map actions.
			$actionsFile=NUC_TMP_DIR.'actions.map';
			if (!is_file($actionsFile) || $rebuild)
			{
				if (!$rebuild)
				{
					print "\033[0;33mActions have not been mapped. Mapping actions...\033[0m".PHP_EOL;
				}
				else
				{
					print "\033[0;33mRemapping action...\033[0m".PHP_EOL;
				}
				
				$this->_mapActions(Vector{ROOT_DIR},false);
			}
			
			//Attempt to generate the keypair.
			if ($configPath)
			{
				$config=Config::getInstance($configPath);
				$cryptConfig=$config->get('crypt');
				if (!is_null($cryptConfig))
				{
					$crypt=CryptHelper::getInstance($config->get('crypt'));
					if ($regenKeypair || $crypt->doesKeypairExist()===0)
					{
						print "\033[0;32mGenerating keypair...\033[0m".PHP_EOL;
						$crypt->generateKeypair();
						print "\033[0;32mCompleted generating keypair.\033[0m".PHP_EOL;
					}
				}
				else
				{
					print "\033[0;33mWarning! No crypt config set. I am unable to create keyparis.\033[0m".PHP_EOL;
				}
			}
			else
			{
				print "\033[0;33mWarning! I was unable to load the config path.\033[0m".PHP_EOL;
				print "\033[0;33mThis means I wasn't able to generate a keypair.\033[0m".PHP_EOL;
			}
			
			print "\033[0;32mNuclio setup complete. Have fun and code responsibly!\033[0m".PHP_EOL;
			exit(0);
		}
		
		<<
			command('providers::map'),
			arguments
			(
				[
					'--path',
					'The path to scan for classes which have providers.',
					true
				]
			)
		>>
		public function mapProviders(Command $command):void
		{
			$providersFile=NUC_TMP_DIR.'providers.map';
			if (!is_writable(NUC_TMP_DIR))
			{
				throw new ConsoleException(sprintf('The Nuclio cache directory at "%s" is not writable.',NUC_CACHE_DIR));
			}
			if (is_file($providersFile) && !is_writable($providersFile))
			{
				throw new ConsoleException(sprintf('The providers cachefile at "%s" is not writable.',$providersFile));
			}
			$paths=$command->getArgumentValues('path');
			$normalizedPaths=new Vector(null);
			if (!is_null($paths))
			{
				foreach ($paths as $path)
				{
					if (substr($path,0,1)!='/')
					{
						$path=realpath(getcwd()._DS_.$path);
					}
					$normalizedPaths[]=$path;
				}
			}
			$manager=ProviderManager::getInstance();
			$manager->bindPaths($normalizedPaths);
			$providers=$manager->searchForProviders(true);
			if (file_put_contents($providersFile,json_encode($providers))!==false)
			{
				print "\033[0;32mSuccess!\033[0m".PHP_EOL;
				exit(0);
			}
			else
			{
				print 'Oops! Something went wrong. Failed to write to disk dispite the location being writable.'.PHP_EOL;
				exit(1);
			}
		}
		
		<<
			command('providers::reflect'),
			arguments
			(
				[
					['--file','-f'],
					'The file path which contains the class to reflect.',
					true
				],
				[
					['--silent','-s'],
					'Disables error output.',
					false
				]
			)
		>>
		public function reflectForProvider(Command $command):noreturn
		{
			$file	=$command->getArgumentValue('file');
			$silent	=$command->getArgumentValue('silent');
			try
			{
				if (is_file($file))
				{
					$normalizedFile=$file;
					if (substr($file,0,1)!='/')
					{
						$normalizedFile=realpath(getcwd()._DS_.$file);
					}
					if (!is_file($normalizedFile))
					{
						throw new ConsoleException(sprintf('Invalid file argument. "%s" is not a file.',$file));
					}
					if (!is_readable($normalizedFile))
					{
						throw new ConsoleException(sprintf('The file "%s" is not readable.',$normalizedFile));
					}
					$attributes=$this->reflectAttributes($normalizedFile);
					// var_dump($attributes);
					if (is_null($attributes))
					{
						throw new Exception(sprintf('Unable to reflect attributes for %s',$normalizedFile));
					}
					
					$diff=$this->reflectClassInFile($normalizedFile);
					if (is_null($diff))
					{
						throw new Exception(sprintf('Unable to get class info for %s',$normalizedFile));
					}
					
					$classInfo	=ObjectHelper::getClassInfoFromPath($diff);
					$classAttributes=$attributes->get('class');
					if (!is_null($classAttributes) && $classAttributes->containsKey('provides'))
					{
						print serialize([$classInfo['fullName'],$classAttributes->get('provides')]);
					}
				}
				else
				{
					throw new ConsoleException('Invalid file argument.');
				}
			}
			catch (\Exception $exception)
			{
				if (!$silent)
				{
					throw $exception;
				}
				exit(1);
			}
			exit(0);
		}
		
		<<
			command('actions::map'),
			arguments
			(
				[
					'--path',
					'The path to scan for classes which have actions.',
					true
				],
				[
					'--silent',
					'Disables console output.',
					false
				]
			)
		>>
		public function mapActions(Command $command):void
		{
			$paths			=$command->getArgumentValues('path');
			$silent			=$command->getArgumentValue('silent') ?? false;
			$this->_mapActions($paths,$silent);
			exit(0);
		}
		
		private function _mapActions(Vector<string> $paths, bool $silent=false):void
		{
			$actionsMapFile	=NUC_TMP_DIR.'actions.map';
			$actionsMap		=Map{};
			$nuclioExec		='hhvm '.realpath(NUC_BIN);
			$normalizedPaths=Vector{};
			
			if (!is_null($paths))
			{
				foreach ($paths as $path)
				{
					if (substr($path,0,1)!='/')
					{
						$path=realpath(getcwd()._DS_.$path);
					}
					$normalizedPaths[]=$path;
				}
			}
			foreach ($normalizedPaths as $path)
			{
				$results=null;
				chdir($path);
				exec('grep -r --include="*.hh" "action("',&$results);
				print "Mapping actions in \"\033[0;33m".$path."\033[0m\"".PHP_EOL;
				
				$filesProcessed=Vector{};
				
				foreach ($results as $result)
				{
					list($filePath)	=explode(':',$result);
					$filePath		=realpath($filePath);
					if ($filePath===__FILE__)
					{
						continue;
					}
					if ($filesProcessed->linearSearch($filePath)===-1)
					{
						$filesProcessed->add($filePath);
					}
					else
					{
						continue;
					}
					print "\033[0;32mFound Actions in \033[0;92m".$filePath."\033[0m".PHP_EOL;
					$process=Process::getInstance
					(
						$nuclioExec,
						Vector
						{
							'actions::reflect',
							// '--silent',
							'--file '.$filePath
						}
					);
					$output=$process->getOutput();
					if ($output!=='' && !stristr($output,'error') && !stristr($output,'exception'))
					{
						$output		=unserialize($output);
						$methods	=$output[1];
						
						if (!$silent)
						{
							print "Found the following actions in class \033[0;96m".$output[0]."\033[0m:".PHP_EOL;
						}
						foreach ($methods as $methodName=>$actions)
						{
							foreach ($actions as $action)
							{
								if (!$actionsMap->containsKey($action))
								{
									$actionsMap->set($action,Vector{});
									$actionsMap[$action]=Map
									{
										'class'		=>$output[0],
										'method'	=>$methodName
									};
									if (!$silent)
									{
										print "\033[0;34m * \033[0;96m".$action."\033[0;39m attached to method \033[0;96m".$methodName."\033[0m".PHP_EOL;
									}
								}
								else
								{
									$parts		=explode('\\',$output[0]);
									$namespace	=end($parts);
									$message	=<<<MESSAGE
\033[0;31mError! Conflicting action detected. \033[0m
\033[38;5;208m"{$action}" was previously defined. I cannot continue until you fix this.
One of the actions must be changed.
I Suggest you rename or give it a namespace (ie. {$namespace}.{$action} ).
\033[0m
MESSAGE;
									print $message.PHP_EOL;
									exit(1);
								}
							}
						}
					}
					else if (!$silent)
					{
						print "\033[0;31mWarning! File is invalid. Actions will not be available from ".$filePath."\033[0m".PHP_EOL;
						if (strlen($output))
						{
							print "\033[0;33mI was able to capture the following error:\033[0m".PHP_EOL;
							print "\033[1;41m".PHP_EOL.PHP_EOL.trim($output).PHP_EOL."\033[0m".PHP_EOL.PHP_EOL;
						}
					}
				}
			}
			
			if (file_put_contents($actionsMapFile,serialize($actionsMap))!==false)
			{
				print "\033[0;32mActions Mapped!\033[0m".PHP_EOL;
			}
			else
			{
				print 'Oops! Something went wrong. Failed to write to disk dispite the location being writable.'.PHP_EOL;
				exit(1);
			}
		}
		
		<<
			command('actions::reflect'),
			arguments
			(
				[
					['--file','-f'],
					'The file path which contains the class to reflect.',
					true
				],
				[
					['--silent','-s'],
					'Disables error output.',
					false
				]
			)
		>>
		public function reflectForActions(Command $command):void
		{
			$file	=$command->getArgumentValue('file');
			$silent	=$command->getArgumentValue('silent');
			try
			{
				if (is_file($file))
				{
					$normalizedFile=$file;
					if (substr($file,0,1)!='/')
					{
						$normalizedFile=realpath(getcwd()._DS_.$file);
					}
					if (!is_file($normalizedFile))
					{
						throw new ConsoleException(sprintf('Invalid file argument. "%s" is not a file.',$file));
					}
					if (!is_readable($normalizedFile))
					{
						throw new ConsoleException(sprintf('The file "%s" is not readable.',$normalizedFile));
					}
					$attributes	=$this->reflectAttributes($normalizedFile);
					
					if (is_null($attributes))
					{
						throw new Exception(sprintf('Unable to reflect attributes for %s',$normalizedFile));
					}
					
					$diff=$this->reflectClassInFile($normalizedFile);
					if (is_null($diff))
					{
						throw new Exception(sprintf('Unable to get class info for %s',$normalizedFile));
					}
					
					$classInfo=ObjectHelper::getClassInfoFromPath($diff);
					$methodAttributes=$attributes->get('methods');
					$actions=Map{};
					
					foreach ($methodAttributes as $methodName=>$method)
					{
						if ($method->containsKey('action'))
						{
							//Can be many.
							$theseActions=$method->get('action');
							if (is_array($theseActions))
							{
								$actions->set($methodName,new Vector($theseActions));
							}
							else
							{
								$actions->set($methodName,Vector{$theseActions});
							}
						}
					}
					print serialize([$classInfo['fullName'],$actions]);
				}
				else
				{
					throw new ConsoleException('Invalid file argument.');
				}
			}
			catch (\Exception $exception)
			{
				if (!$silent)
				{
					throw $exception;
				}
				exit(1);
			}
			exit(0);
		}
		
		<<
			command('hhvm::watch'),
			arguments
			(
				[
					'--path',
					'The file path to begin the hh_client watching at.',
					false
				]
			)
		>>
		public function hhvmWatch(Command $command):void
		{
			$path=$command->getArgumentValue('path');
			if (is_null($path))
			{
				$path=getcwd();
			}
			if (substr($path,0,1)!='/')
			{
				$path=realpath(getcwd()._DS_.(string)$path);
			}
			chdir($path);
			while (true)
			{
				system('clear');
				print passthru('hh_client');
				sleep(2);
			}
			exit(0);
		}
		
		<<
			command('encryptString'),
			arguments
			(
				[
					['--config','-c'],
					'Path to config files.',
					true
				],
				[
					['--input','-i'],
					'The string to encrypt.',
					true
				],
				[
					['--syslock','-s'],
					'Enable this option to restrict the output to only work on this server.',
					false
				]
			)
		>>
		public function encryptString(Command $command)
		{
			$config	=realpath($command->getArgumentValue('config')).'/';
			$config	=Config::getInstance($config);
			$input	=$command->getArgumentValue('input');
			$syslock=$command->getArgumentValue('syslock');
			$crypt	=CryptHelper::getInstance($config->get('crypt'));
			print $crypt->encrypt($input).PHP_EOL;
			exit(0);
		}
		
		<<command('runTests')>>
		public function runTests(Command $command)
		{
			PHPUnit_TextUI_Command::main();
			exit(0);
		}
		
		private function reflectAttributes(string $file):?Map<string,mixed>
		{
			if (is_file($file))
			{
				$diff=$this->reflectClassInFile($file);
				if (!is_null($diff))
				{
					//Standards dictate that only 1 class will exist in the file. So assume it'll be the only new file.
					$classInfo=ObjectHelper::getClassInfoFromPath($diff);
					$reflection=new ReflectionClass($classInfo['fullName']);
					$attributes=Map
					{
						'class'=>new Map($reflection->getAttributes()),
						'methods'=>Map{}
					};
					$methods=Map{};
					foreach ($reflection->getMethods() as $method)
					{
						$methodAttributes=new map($method->getAttributes());
						$methods->set($method->getName(),$methodAttributes);
					}
					$attributes->set('methods',$methods);
					return $attributes;
				}
			}
			else
			{
				throw new ConsoleException('Invalid file argument.');
			}
			return null;
		}
		
		private function doesNUCTempDirExist():bool
		{
			return is_dir(NUC_TMP_DIR);
		}
		
		private function createNUCTempDir(?string $user=null, ?string $group=null):void
		{
			if (!$this->doesNUCTempDirExist())
			{
				$tmpDir=NUC_TMP_DIR;
				if (is_null($user))
				{
					$user=self::DEFAULT_USER;
				}
				if (is_null($group))
				{
				
					$group=self::DEFAULT_GROUP;
				}
				print "\033[0;33mCreating nuclio temp dir at \"".$tmpDir."\"\033[0m".PHP_EOL;
				mkdir($tmpDir);
				$process=Process::getInstance('chown '.$user.':'.$group.' '.$tmpDir);
				$error	=$process->getError();
				if (strstr($error,'Operation not permitted') || !is_writable($tmpDir))
				{
					print <<<MESSAGE
\e[m\e[1;41mThere is a problem setting permissions on the nuclio temp dir "{$tmpDir}".
* Try running this command as sudo.
* Note that you may need to specify the --user and/or --group flags if you want Nuclio to configure the permissions for you.\e[m

MESSAGE;
					rmdir($tmpDir);
					exit(1);
				}
			}
		}
		
		
		private function doesNUCLogsDirExist():bool
		{
			return is_dir(NUC_LOG_DIR);
		}
		
		private function createNUCLogsDir(?string $user=null, ?string $group=null):void
		{
			if (!$this->doesNUCLogsDirExist())
			{
				$logDir=NUC_LOG_DIR;
				if (is_null($user))
				{
					$user=self::DEFAULT_USER;
				}
				if (is_null($group))
				{
					$group=self::DEFAULT_GROUP;
				}
				print "\033[0;33mCreating nuclio log dir at \"".$logDir."\"\033[0m".PHP_EOL;
				mkdir($logDir);
				$process=Process::getInstance('chown '.$user.':'.$group.' '.$logDir);
				$error	=$process->getError();
				if (strstr($error,'Operation not permitted') || !is_writable($logDir))
				{
					print <<<MESSAGE
\e[m\e[1;41mThere is a problem setting permissions on the nuclio logs dir "{$logDir}".
* Try running this command as sudo.
* Note that you may need to specify the --user and/or --group flags if you want Nuclio to configure the permissions for you.\e[m

MESSAGE;
					rmdir($logDir);
					exit(1);
				}
			}
		}
		
		private function doesNUCCacheDirExist():bool
		{
			return is_dir(NUC_CACHE_DIR);
		}
		
		private function createNUCCacheDir(?string $user=null, ?string $group=null):void
		{
			if (!$this->doesNUCCacheDirExist())
			{
				$cacheDir=NUC_CACHE_DIR;
				if (is_null($user))
				{
					$user=self::DEFAULT_USER;
				}
				if (is_null($group))
				{
					$group=self::DEFAULT_GROUP;
				}
				print "\033[0;33mCreating nuclio cache dir at \"".$cacheDir."\"\033[0m".PHP_EOL;
				mkdir($cacheDir);
				$process=Process::getInstance('chown '.$user.':'.$group.' '.$cacheDir);
				$error	=$process->getError();
				if (strstr($error,'Operation not permitted') || !is_writable($cacheDir))
				{
					print <<<MESSAGE
\e[m\e[1;41mThere is a problem setting permissions on the nuclio cache dir "{$cacheDir}".
* Try running this command as sudo.
* Note that you may need to specify the --user and/or --group flags if you want Nuclio to configure the permissions for you.\e[m

MESSAGE;
					rmdir($cacheDir);
					exit(1);
				}
			}
		}
				
		private function reflectClassInFile(string $file):?string
		{
			if ($this->diffedClasses->containsKey($file))
			{
				return $this->diffedClasses->get($file);
			}
			$tokens		=file_get_contents($file) |> token_get_all($$);
			$namespace	='';
			$class		=null;
			for ($i=0,$j=count($tokens); $i<$j; $i++)
			{
				if (is_array($tokens[$i]))
				{
					switch ($tokens[$i][0])
					{
						//Handle namespace
						case T_NAMESPACE:
						{
							//Loop fetching each string token until reaching an ending token.
							for ($k=$i+1; $k<$j; $k++)
							{
								//Keep following the string tokens.
								if ($tokens[$k][0]===T_STRING)
								{
									$namespace.='\\'.$tokens[$k][1];
								}
								//Find the end of the namespace declaration.
								else if ($tokens[$k]==='{' || $tokens[$k]===';')
								{
									$i=$k+1;
									break; //break for
								}
							}
							break; //break switch
						}
						//Handle class.
						case T_CLASS:
						{
							$class=$tokens[$i+2][1];
							break; // break switch
						}
					}
				}
				if (!is_null($class))
				{
					break; //break for
				}
			}
			if (!is_null($class))
			{
				$namespace=rtrim($namespace,'\\').'\\';
				$class=$namespace.$class;
				$this->diffedClasses->set($file,$class);
			}
			else
			{
				$this->diffedClasses->set($file,null);
			}
			return $class;
		}
		
		
		<<
		command('db::build'),
		arguments
		(
			[
				['--config','-c'],
				'The path to the config file you to use for this command.',
				false
			],
			[
				['--silent','-s'],
				'Disables output.',
				false
			]
		)
		>>
		public function dbBuild(Command $command):void
		{
			$config				=($command->getArgumentValue('config') ?? NUC_DEFUALT_CONFIG_DIR)
								|>Config::getInstance($$);
			$silent				=$command->getArgumentValue('silent') ?? false;
			$modelMapFile		=NUC_TMP_DIR.'models.map';
			$modelMap			=Map{};
			$root				=realpath(ROOT_DIR);
			$iterator			=new RecursiveIteratorIterator(new RecursiveDirectoryIterator($root));
			
			CryptHelper::getInstance($config->get('crypt'));
			$this->connectDataSources($config);
			
			if (is_file($modelMapFile))
			{
				$modelMap	=file_get_contents($modelMapFile)
									|>unserialize($$);
			}
			
			
			foreach($iterator as $fullPath=>$object)
			{
				if ($object->isFile()
				&& !strstr($fullPath, '/bin/')
				&& !strstr($fullPath, '/public/')
				&& $object->getExtension()=='hh')
				{
					$nuclioExec='hhvm '.realpath(NUC_BIN);
					$process=Process::getInstance
					(
						$nuclioExec,
						Vector
						{
							'reflect',
							'--file '.$fullPath
						}
					);
					$output=$process->getOutput();
					
					if ($output!=='' && !stristr($output,'error') && !stristr($output,'exception'))
					{
						$reflection=unserialize($output);
						if ($reflection->isSubclassOf('nuclio\plugin\database\orm\Model')
						&& !$reflection->isSubclassOf('nuclio\plugin\database\orm\DynamicModel'))
						{
							if (!$silent)
							{
								print sprintf
								(
									"Found model \033[0;96m%s\033[0m in file \033[0;96m%s\033[0m",
									$reflection->getName(),
									$fullPath
								).PHP_EOL;
							}
							$annotationReader	=Reader::getInstance($reflection->getName());
							$this->_annotations	=$annotationReader->getAnnotations();
							$classAnnotations	=$this->_annotations->get('class');
							if (!is_null($classAnnotations))
							{
								invariant($classAnnotations instanceof Map, 'must be a Map.');
								$collection		=(string)$classAnnotations->get('collection');
								print sprintf
								(
									"This model references \033[0;96m%s\033[0m as a collection/table.",
									$collection
								).PHP_EOL;
								
								$conflict=false;
								$alreadyMapped=false;
								if ($modelMap->containsKey($fullPath))
								{
									$alreadyMapped=true;
								}
								else
								{
									//Search the model map to find any reference to this collection.
									foreach ($modelMap as $fileName=>$metaData)
									{
										if ($metaData->get('collection')===$collection)
										{
											$message=<<<MESSAGE
\033[0;31mWarning! Collection/Table conflict.
This collection has already been defined in another model. It will be ignored.
\033[0;31mThe conflicting model is \033[0m%s
\033[0;31mIt is located in \033[0m%s
\033[0m
MESSAGE;
										
											print sprintf
											(
												$message,
												$metaData->get('class'),
												$fileName
											);
											$conflict=true;
											break;
										}
									}
								}
								if (!$conflict)
								{
									$instance=$reflection->newInstance();
									if (!$alreadyMapped)
									{
										$metaData=ImmMap
										{
											'class'		=>$reflection->getName(),
											'collection'=>$collection,
											'fields'	=>$instance->getFields()
										};
										$modelMap->set($fullPath,$metaData);
									}
									//Create Collection/Table.
									$source=DataSourceManager::getSource('primary');
									$driver=$source->getDriver($source);
									
									if (!$driver->collectionExists($collection))
									{
										print "\033[0;32mCreating collection/table.\033[0m".PHP_EOL;
										$driver->createCollection($instance);
									}
									else
									{
										//TODO: Check for changes.
										print "\033[0;32mCollection/table already exists.\033[0m".PHP_EOL;
										
										$mappedFields	=$modelMap->get($fullPath)?->get('fields');
										$instanceFields	=$instance->getFields();
										$additions		=array_diff($instanceFields,$mappedFields);
										$removals		=array_diff($mappedFields,$instanceFields);
										$totalChanges	=count($additions)+count($removals);
										if ($totalChanges>0)
										{
											print sprintf("Found \033[0;96m%s\033[0m changes since this script was last run.".PHP_EOL,$totalChanges);
											print sprintf("\033[0;96m%s\033[0m addition(s)".PHP_EOL,count($additions));
											print sprintf("\033[0;96m%s\033[0m removal(s)".PHP_EOL,count($removals));
											print "The following changes would be made:".PHP_EOL;
											
											foreach ($additions as $addition)
											{
												if (!$driver->columnExists($instance,'__'.$addition))
												{
													print sprintf("Column \033[0;96m%s\033[0m will be 033[0;96mCREATED\033[0m.".PHP_EOL,$addition);
												}
												else
												{
													print sprintf("Column \033[0;96m%s\033[0m already existed in the past and will be \033[0;96mRESTORED\033[0m.".PHP_EOL,$addition);
												}
											}
											foreach ($removals as $removal)
											{
												print sprintf("Column \033[0;96m%s\033[0m will be \033[0;96mRENAMED\033[0m and set to \033[0;96mNULLABLE\033[0m but \033[0;96mWILL NOT\033[0m be removed.".PHP_EOL,$removal);
											}
											$answer=$this->confirm("\033[0;33mDo you want to proceed with the above changes?\033[0m");
											if ($answer)
											{
												$modelMap->get($fullPath)
														?->toMap()
														->set('fields',$instanceFields)
														->toImmMap()
														|>$modelMap->set($fullPath,$$);
												
												foreach ($additions as $addition)
												{
													if (!$driver->columnExists($instance,'__'.$addition))
													{
														$driver->alterColumn($instance,$addition,'add');
														print sprintf("\033[0;32mColumn \033[0;96m%s\033[0;32m was \033[0;96mADDED\033[0m.".PHP_EOL,$addition);
													}
													else
													{
														$driver->alterColumn($instance,$addition,'restore');
														print sprintf("\033[0;32mColumn \033[0;96m%s\033[0;32m was \033[0;96mRESTORED\033[0m.".PHP_EOL,$addition);
													}
												}
												foreach ($removals as $removal)
												{
													$driver->alterColumn($instance,$removal,'remove');
													print sprintf("\033[0;32mColumn \033[0;96m%s\033[0;32m was \033[0;96mRENAMED\033[0m.".PHP_EOL,$removal);
												}
											}
											else
											{
												print "\033[0;32mNo changes will be made.\033[0m".PHP_EOL;
											}
										}
										else
										{
											print "\033[0;32mNo changes found.\033[0m".PHP_EOL;
										}
									}
								}
							}
							else
							{
								print "\033[0;31mWarning! Class is a model but has no annotations.\033[0m";
							}
						}
					}
					else if (!$silent)
					{
						print "\033[0;31mWarning! File is invalid. Model could not be read from ".$fullPath."\033[0m".PHP_EOL;
						if (strlen($output))
						{
							print "\033[0;33mI was able to capture the following error:\033[0m".PHP_EOL;
							print "\033[1;41m".PHP_EOL.PHP_EOL.trim($output).PHP_EOL."\033[0m".PHP_EOL.PHP_EOL;
						}
					}
				}
			}
			
			if (file_put_contents($modelMapFile,serialize($modelMap))!==false)
			{
				print "\033[0;32mModels Mapped!\033[0m".PHP_EOL;
			}
			else
			{
				print 'Oops! Something went wrong. Failed to write to disk dispite the location being writable.'.PHP_EOL;
				exit(1);
			}
			
		}
		
		<<
			command('reflect'),
			arguments
			(
				[
					['--file','-f'],
					'The file path which contains the class to reflect.',
					true
				],
				[
					['--silent','-s'],
					'Disables output.',
					false
				]
			)
		>>
		public function reflect(Command $command):void
		{
			$file	=$command->getArgumentValue('file');
			$silent	=$command->getArgumentValue('silent') ?? false;
			
			try
			{
				if (is_file($file))
				{
					$normalizedFile=$file;
					if (substr($file,0,1)!='/')
					{
						$normalizedFile=realpath(getcwd()._DS_.$file);
					}
					if (!is_file($normalizedFile))
					{
						throw new ConsoleException(sprintf('Invalid file argument. "%s" is not a file.',$file));
					}
					if (!is_readable($normalizedFile))
					{
						throw new ConsoleException(sprintf('The file "%s" is not readable.',$normalizedFile));
					}
					
					$fullClassName=$this->reflectClassInFile($normalizedFile);
					if (is_null($fullClassName))
					{
						throw new Exception(sprintf('Unable to get class info for %s',$normalizedFile));
					}
					print new ReflectionClass($fullClassName) |> serialize($$);
				}
				else
				{
					throw new ConsoleException('Invalid file argument.');
				}
			}
			catch (\Exception $exception)
			{
				if (!$silent)
				{
					throw $exception;
				}
				exit(1);
			}
			exit(0);
		}
		
		private function connectDataSources(Config $config):void
		{
			$connections=$config->get('database.connections');
			$manager=DataSourceManager::getInstance();
			if ($connections instanceof KeyedTraversable)
			{
				foreach ($connections as $connectionRef=>$connection)
				{
					$manager->createConnection($connectionRef,new Map($connection));
				}
			}
		}
		
		
		private function confirm(string $message):bool
		{
			$endLine="<\033[0;96my/yes\033[0m> <\033[0;96mn/no\033[0m>: ";
			print $message.PHP_EOL;
			while (true)
			{
				$answer=readline($endLine);
				switch ($answer)
				{
					case 'y':
					case 'yes':
					{
						return true;
					}
					case 'n':
					case 'no':
					{
						return false;
					}
				}
			}
		}
	}
}