<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\helper
{
	use nuclio\core\ClassManager;
	
	<<singleton>>
	class CryptHelper
	{
		const int VECTOR_SIZE			=16;
		const string ENCRYPTION_METHOD	='aes-256-cbc';
		const string SEPARATOR			='[::]';
		
		private Map<string,string> $config=Map{};
		
		public static function getInstance(...$args):CryptHelper
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(Map<string,string> $options)
		{
			$this->config=$options;
		}
		
		public function encrypt(string $input, bool $syslock=false):string
		{
			$this->generateKeypair();
			
			$keyDir			=$this->config->get('keyDir');
			if (substr($keyDir,0,1)!='/')
			{
				$keyDir=realpath(ROOT_DIR.$keyDir);
			}
			if (substr($keyDir,-1,1)!='/')
			{
				$keyDir.='/';
			}			
			
			$publicKeyPath	=$keyDir.$this->config->get('publicKey');
			$publicKey		=openssl_pkey_get_public(file_get_contents($publicKeyPath));
			$vector			=mcrypt_create_iv(self::VECTOR_SIZE,MCRYPT_DEV_URANDOM);
			$hashedString	=openssl_encrypt
							(
								$input,
								self::ENCRYPTION_METHOD,
								$this->config->get('salt'),
								0,
								$vector
							).base64_encode($vector);
			$encryptedString=null;
			
			//Handle syslocking the string.
			if ($syslock)
			{
				$syslockHash=$this->getSyslockHash();
				$hashedString.=$syslockHash;
			}
			
			//Encrypt the string.
			openssl_public_encrypt($hashedString,$encryptedString,$publicKey);
			
			return base64_encode($encryptedString);
		}
		
		public function decrypt(string $input, bool $syslock=false):?string
		{
			$input			=base64_decode($input);
			$keyDir			=$this->config->get('keyDir');
			if (substr($keyDir,0,1)!='/')
			{
				$keyDir=realpath(ROOT_DIR.$keyDir);
			}
			if (substr($keyDir,-1,1)!='/')
			{
				$keyDir.='/';
			}
			$privateKeyPath	=$keyDir.$this->config->get('privateKey');
			$privateKey		=openssl_pkey_get_private(file_get_contents($privateKeyPath));
			$decryptedString=null;
			
			//Decrypt the input.
			openssl_private_decrypt($input,$decryptedString,$privateKey);
			
			if (is_null($decryptedString) || !strstr($decryptedString,'=='))
			{
				return null;
			}
			
			list($hash,$vector,$syslockHash)=explode('==',$decryptedString);
			
			$hash.='==';
			$vector.='==';
			
			//Decrpyt the hashed string.
			$output=openssl_decrypt
			(
				$hash,
				self::ENCRYPTION_METHOD,
				$this->config->get('salt'),
				0,
				base64_decode($vector)
			);
			
			if (!$syslock && (bool)strlen($syslockHash))
			{
				return null;
			}
			
			if ($syslock && $syslockHash!==$this->getSyslockHash())
			{
				return null;
			}
			return $output;
		}
		
		private function getSyslockHash():string
		{
			$DMI	=exec('sudo dmidecode -t 4 | grep ID | sed \'s/.*ID://;s/ //g\'');
			$MAC	=exec('ifconfig | grep -oP \'HWaddr \K.*\' | sed \'s/://g\'');
			
			return hash('sha256',$DMI.' '.$MAC.PHP_EOL).PHP_EOL;
		}
		
		public function generateKeypair(bool $regen=false):void
		{
			$keyDir			=$this->config->get('keyDir');
			$privateKeyPath	=$keyDir.$this->config->get('privateKey');
			$publicKeyPath	=$keyDir.$this->config->get('publicKey');
			
			//Generate SSH keys.
			if ($regen || !file_exists($privateKeyPath))
			{
				//PRIVATE
				exec(sprintf('openssl genrsa -out %s 2048 > /dev/null 2>&1',$privateKeyPath));
			}
			
			if ($regen || !file_exists($publicKeyPath))
			{
				//PUBLIC
				exec(sprintf('openssl rsa -in %s -out %s -outform PEM -pubout > /dev/null 2>&1',$privateKeyPath,$publicKeyPath));
			}
		}
		
		public function doesKeypairExist():int
		{
			$keyDir			=$this->config->get('keyDir');
			$privateKeyPath	=$keyDir.$this->config->get('privateKey');
			$publicKeyPath	=$keyDir.$this->config->get('publicKey');
			$return			=0;
			
			if (file_exists($privateKeyPath))
			{
				$return++;
			}
			if (file_exists($publicKeyPath))
			{
				$return++;
			}
			return $return;
		}
	}
}
