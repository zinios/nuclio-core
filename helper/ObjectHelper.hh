<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\helper
{
	use nuclio\core\exception\HelperException;

	// use \ArrayObject;
	
	type CLASS_INFO=shape
	(
		'namespace'	=>string,
		'className'	=>string,
		'fullName'	=>string
	);
	
	/**
	 * The Object helper class.
	 * 
	 * This class has many specialized methods for dealing 
	 * with classes and objects.
	 *  
	 * @author 
	 * @package nuclio
	 * @static
	 */
	class ObjectHelper
	{
		/**
		 * @var Integer
		 */
		const CLASSINFO_NAMESPACE=1;
		
		/**
		 * @var Integer
		 */
		const CLASSINFO_CLASSNAME=2;
		// /**
		//  * @var String
		//  */
		// const CLASSINFO_VAL_NAMESPACE = 'namespace';
		// /**
		//  * @var String
		//  */
		// const CLASSINFO_VAL_CLASSNAME = 'classname';
		
		
		/**
		 * Gets the base class name for a given class.
		 * 
		 * @access public
		 * @static
		 * @param Mixed $className - Either the class name or an instance of the class.
		 * @return String - The base class name
		 */
		public static function getBaseClassName(mixed $className):?string
		{
			if (is_object($className))
			{
				$className=get_class($className);
			}
			if (is_string($className))
			{
				return self::getClassInfo($className)['className'];
			}
			return null;
		}
		
		/**
		 * Gets the namespace for a given class.
		 * 
		 * @access public
		 * @static
		 * @param Mixed $className - Either the class name or an instance of the class.
		 * @return String
		 */
		public static function getNamespace(mixed $className):?string
		{
			if (is_object($className))
			{
				$className=get_class($className);
			}
			if (is_string($className))
			{
				return self::getClassInfo($className)['namespace'];
			}
			return null;
		}
		
		// /**
		//  * Locates the source directory for specified class.
		//  * Note: it does only work with native plugins.
		//  * 
		//  * @access public
		//  * @static
		//  * @param Mixed $className - Either the class name or an instance of the class.
		//  * @return String the physical path to the directory containing the class. 
		//  */
		// public static function getClassPath($className)
		// {
		// 	if (is_object($className))
		// 	{
		// 		$className=get_class($className);
		// 	}
		// 	$root=(strstr($className,'nuclio'))?NUC_HOME:APP_HOME;
		// 	return $root . str_replace(
		// 		array(
		// 			'application\\',
		// 			'nuclio\\', 
		// 			'\\'
		// 		), 
		// 		array(
		// 			'',
		// 			'', 
		// 			_DS_
		// 		),  
		// 		self::getNamespace($className)
		// 	) . _DS_;
		// }
		
		/**
		 * This helper function returns an associative array containing information about the $classname
		 * 
		 * @access public
		 * @static
		 * @param String $className the class being investigated
		 * @return array an associative array of information corresponding to the following keys: 
		 * * namespace
		 * * classname
		 * In case the string cannot be parsed, null is returned.
		 */
		public static function getClassInfo(string $className):CLASS_INFO
		{
			$info=shape
			(
				'namespace'	=>'',
				'className'	=>'',
				'fullName'	=>''
			);
			
			//checking the pattern
			$matches=[];
			if (preg_match('/^((?:[^\\\\]+\\\\)*)([^\\\\]+)$/', $className, &$matches))
			{
				$info['namespace']	=rtrim($matches[1], '\\');
				$info['className']	=$matches[2];
				$info['fullName']	=$matches[1].$matches[2];
			}
			
			return $info;
		}
		
		/**
		 * This helper function returns an associative array containing information about the $classPath
		 * 
		 * @access public
		 * @static
		 * @param String $classPath the class being investigated
		 * @return array an associative array of information corresponding to the following keys: 
		 * * namespace
		 * * classname
		 * In case the string cannot be parsed, null is returned.
		 */
		public static function getClassInfoFromPath(string $classPath):CLASS_INFO
		{
			// var_dump($classPath);
			$className=str_replace
			(
				[
					realpath(NUC_DIR),
					realpath(ROOT_DIR),
					'/',
					'.hh',
					'.php'
				],
				[
					'nuclio',
					'',
					'\\',
					'',
					''
				],
				$classPath
			);
			// $className=ltrim($className,'\\');
			// return self::getClassInfo($className);
			return ltrim($className,'\\') |> self::getClassInfo($$);
		}
		
		public static function uses(mixed $class,string $trait):bool
		{
			if (is_object($class))
			{
				$class=get_class($class);
			}
			return (class_uses($class) |> in_array($trait,$$));
		}
	}
}
